
export class Category {
  static REPAS = 'REPAS';
  static PATISSERIE = 'PATISSERIE';
  static EPICERIE = 'EPICERIE';
  static AUTRES = 'AUTRES';
}
