import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';
import { Storage } from '../constants/storage.constants';
import { JwtInterceptor } from './jwt.interceptor';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  user$ = new BehaviorSubject<User>(null);

  constructor(private http: HttpClient,
              private jwt: JwtInterceptor,
              private router: Router,
              private jwtHelper: JwtHelperService) {
    this.getUserSession();
  }

  /**
   * Créer un compte utilisateur
   * @param email   Identifiant de l'utilisateur
   * @param name    Nom, prénom ou pseudo
   * @param password  Mot de passe
   */
  register(email: string, name: string, password: string): Observable<User> {
    const body = {email, name, password};
    return this.http.post<User>(`${environment.baseURL}/register`, body);
  }

  /**
   * S'authentifier sur l'application
   * @param email     Identifiant de l'utilisateur
   * @param password  Mot de passe
   */
  authenticate(email: string, password: string): Observable<User> {
    const body = {email, password};
    return this.http.post<User>(`${environment.baseURL}/auth`, body)
      .pipe(
        tap(u => this.storeUserSession(u))
      );
  }

  /**
   * Sauvegarde l'utilisateur authentifié en session
   * @param user  Utilisateur
   */
  storeUserSession(user: User): void {
    this.broadcastUserWithToken(user);
    sessionStorage.setItem(Storage.USER, JSON.stringify(user));
  }

  /**
   * Récupérer le user depuis la session et le broadcaster par evenement
   */
  getUserSession(): void {
    const value = sessionStorage.getItem(Storage.USER);
    if (value) {
      const user: User = JSON.parse(value);
      this.broadcastUserWithToken(user);
    }
  }

  /**
   * Se déconnecter de l'application
   */
  logout(): void {
    this.user$.next(null);
    this.jwt.removeToken();
    sessionStorage.removeItem(Storage.USER);
    this.router.navigate(['/login']);
  }

  /**
   * Vérifier si un utilisateur est connecté ou non
   */
  isAuthenticated(): boolean {
    return sessionStorage.getItem(Storage.USER) ? true : false;
  }

  /**
   * Broadcast du user authentifié et config JWT
   * @param user
   * @private
   */
  private broadcastUserWithToken(user: User): void {
    this.jwt.addToken(user.token);
    const clearToken = this.jwtHelper.decodeToken(user.token);
    user.roles = clearToken.authorities;
    this.user$.next(user);
  }

  /**
   * Récupérer la valeur instantanée l'utilisateur authentifié
   */
  getUserValue(): User {
    return this.user$.getValue();
  }
}
