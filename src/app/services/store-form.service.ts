import { Injectable } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { StoreModel } from '../models/store.model';

@Injectable()
export class StoreFormService {

  form: FormGroup;
  labels: Array<string> = [];

  /**
   * Validateur pour les mots de passe
   *
   * @param control Champ FormControl sur lequel est ajouté le validator
   */
  static coordinate(control: AbstractControl): ValidationErrors | null {
    const regex = new RegExp('^[0-9]{1,3}[,][0-9]+$');
    return control.value && !regex.test(control.value) ? { coordinate: true } : null;
  }

  constructor(private fb: FormBuilder) {
  }

  /**
   * Initialiser le form de creation de commerce
   * @private
   */
  initForm(labels: Array<string>, store: StoreModel = null): void {
    this.labels = labels;
    this.form = this.fb.group({
      details: this.fb.group({
        name: new FormControl('', [ Validators.required ]),
        description: new FormControl(''),
        category: new FormControl('', [ Validators.required ]),
        labels: this.mapLabelsToCheckboxArrayGroup(labels, store?.labels)
      }),
      location: this.fb.group({
        latitude: new FormControl('', [ Validators.required, StoreFormService.coordinate ]),
        longitude: new FormControl('', [ Validators.required, StoreFormService.coordinate ]),
        address: new FormGroup({
          street: new FormControl('', [ Validators.required ]),
          code: new FormControl('', [ Validators.required, Validators.pattern('^[0-9]*$') ]),
          city: new FormControl('', [ Validators.required ])
        })
      })
    });
    if (store) {
      this.loadStore(store);
    }
  }

  /**
   * Ré-initialiser le formulaire
   */
  reset(): void {
    this.form.reset();
    this.initForm(this.labels);
  }

  /**
   * Mapper les labels pour être compatible avec le composant checkbox de MD
   * @param labels  Liste de labels récupérés depuis le referentiel
   * @param storeLabels Labels du commerce
   * @private
   */
  private mapLabelsToCheckboxArrayGroup(labels: Array<string>, storeLabels: Array<string> = []): FormArray {
    return this.fb.array(labels.map((label: string) => {
      const selected = storeLabels?.includes(label) ?? false;
      return this.fb.group({
        name: this.fb.control(label),
        selected: this.fb.control(selected)
      });
    }));
  }

  /**
   * Charger les données d'un commerce existant
   * @param store
   */
  loadStore(store: StoreModel): void {
    this.form.patchValue({
      details: store,
      location: store.location
    });
  }
}
