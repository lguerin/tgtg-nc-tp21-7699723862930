import { TestBed } from '@angular/core/testing';

import { TgtgService } from './tgtg.service';

describe('TgtgService', () => {
  let service: TgtgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TgtgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
