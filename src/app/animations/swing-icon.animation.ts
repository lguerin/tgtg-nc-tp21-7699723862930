import { animate, AnimationTriggerMetadata, style, transition, trigger } from '@angular/animations';

export const swingIconAnimation: Array<AnimationTriggerMetadata> = [
  trigger('swingIcon', [
    transition(':enter', [
      style({ fontSize: '25px', marginLeft: '-15px' }),
      animate('0.2s', style({ transform: 'rotate(15deg)' })),
      animate('0.2s', style({ transform: 'rotate(-15deg)' })),
      animate('0.2s', style({ transform: 'rotate(15deg)' })),
      animate('0.2s', style({ transform: 'rotate(-15deg)' })),
      animate('0.2s', style({ transform: 'rotate(15deg)' })),
      animate('0.2s', style({ transform: 'rotate(-15deg)' })),
      animate('0.2s', style({ transform: 'rotate(15deg)' })),
      animate('0.2s', style({ transform: 'rotate(-15deg)' })),
      animate('0.2s', style({ transform: 'rotate(15deg)' })),
      animate('0.2s', style({ transform: 'rotate(0deg)' })),
    ])
  ])
];

