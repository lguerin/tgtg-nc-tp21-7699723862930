import { Location } from './location.model';

export interface StoreModel {
  id: string;
  name: string;
  description: string;
  category: string;
  labels: Array<string>;
  capacity: number;
  start?: string;
  end?: string;
  location: Location;
}
