import { Address } from './address.model';

export interface Location {
  longitude: number;
  latitude: number;
  address: Address;
}
