import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  // Mock des services
  const mockAuthenticationService = jasmine.createSpyObj<AuthenticationService>('AuthenticationService', ['authenticate']);
  const mockRouter = jasmine.createSpyObj<Router>('Router', ['navigate']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule ],
      declarations: [ LoginComponent ],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthenticationService },
        { provide: Router, useValue: mockRouter }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it(`Le bouton submit est inactif si le formulaire n'est pas valide`, () => {
    // Given
    const element = fixture.nativeElement;

    // When
    fixture.detectChanges();

    // Then
    expect(element.querySelector('button'))
      .withContext('Le formulaire a un bouton submit')
      .not.toBeNull();

    expect(element.querySelector('button').hasAttribute('disabled'))
      .toBe(true, `Le bouton submit doit etre inactif si le formulaire n'est pas valide`);
  });

  it(`Le bouton submit est actif si le formulaire est valide`, () => {
    // Given
    const element = fixture.nativeElement;

    // When
    const emailInput = element.querySelector('input[name="email"]');
    emailInput.value = 'email@lgdev.fr';
    emailInput.dispatchEvent(new Event('input'));
    const passwordInput = element.querySelector('input[name="password"]');
    passwordInput.value = 'password';
    passwordInput.dispatchEvent(new Event('input'));

    // when we trigger the change detection
    fixture.detectChanges();

    // then we should have a submit button enabled
    expect(emailInput).not.toBeNull(`Le champ 'email' n'existe pas`);
    expect(passwordInput).not.toBeNull(`Le champ 'password' n'existe pas` );
    expect(element.querySelector('button')
      .hasAttribute('disabled'))
      .toBe(false, 'Le bouton submit doit etre actif si le formulaire est valide');
  });

  it(`Le champ 'email' n'est pas valide si le format est incorrect`, () => {
    // Given
    const element = fixture.nativeElement;

    // When
    const emailInput = element.querySelector('input[name="email"]');
    emailInput.value = 'XXX';
    emailInput.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    // Then
    fixture.detectChanges();
    const emailError = element.querySelector('mat-error');
    expect(emailError).withContext(`Une erreur sur le format email doit s'afficher en cas d'erreur`).not.toBeNull();
    expect(emailError.textContent).toBe('Email invalide');
  });
});
