import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { STORES_ROUTES } from './stores.routes';
import { StoresComponent } from './stores.component';
import { StoreComponent } from './store/store.component';
import { FilterComponent } from './filter/filter.component';
import { StoreCardsComponent } from '../store-cards/store-cards.component';
import { StoreCardComponent } from '../store-cards/store-card/store-card.component';
import { BadgeComponent } from '../badge/badge.component';
import { ExpiredSincePipe } from '../../pipes/expired-since.pipe';
import { CapacityColorDirective } from '../../directives/capacity-color.directive';
import { SharedModule } from '../shared/shared.module';
import { StoreFormComponent } from './store/store-form/store-form.component';
import { DetailsFormComponent } from './store/store-form/details-form/details-form.component';
import { LocationFormComponent } from './store/store-form/location-form/location-form.component';
import { AddressFormComponent } from './store/store-form/address-form/address-form.component';

@NgModule({
  declarations: [
    StoresComponent,
    StoreComponent,
    FilterComponent,
    StoreCardsComponent,
    StoreCardComponent,
    BadgeComponent,
    ExpiredSincePipe,
    CapacityColorDirective,
    StoreFormComponent,
    DetailsFormComponent,
    LocationFormComponent,
    AddressFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(STORES_ROUTES)
  ]
})
export class StoresModule { }
