import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { StoreFormService } from '../../../../../services/store-form.service';
import { TranslateService } from '@ngx-translate/core';
import { BaseFormComponent } from '../base-form.component';

@Component({
  selector: 'nc-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['../store-form.component.scss', './address-form.component.scss']
})
export class AddressFormComponent extends BaseFormComponent implements OnInit {

  @Input()
  group: FormGroup;

  constructor(protected storeFormService: StoreFormService,
              protected translateService: TranslateService) {
    super(storeFormService, translateService);
  }

  ngOnInit(): void {
  }

}
