import { StoreFormService } from '../../../../services/store-form.service';
import { AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

export abstract class BaseFormComponent {

  constructor(protected storeFormService: StoreFormService,
              protected translateService: TranslateService) {
  }

  getErrorMessage(control: AbstractControl): string {
    let error = '';
    if (control.hasError('required')) {
      error = this.translateService.instant('validation.error.required');
    }
    if (control.hasError('coordinate')) {
      error = this.translateService.instant('validation.error.coordinate');
    }
    return error;
  }
}
