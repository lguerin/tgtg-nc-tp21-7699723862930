import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { StoreFormService } from '../../../../services/store-form.service';
import { StoreModel } from '../../../../models/store.model';
import { TgtgService } from '../../../../services/tgtg.service';
import { ActivatedRoute } from '@angular/router';
import { ReferentielService } from '../../../../services/referentiel.service';

@Component({
  selector: 'nc-store-form',
  templateUrl: './store-form.component.html',
  styleUrls: ['./store-form.component.scss'],
  providers: [ StoreFormService ]
})
export class StoreFormComponent implements OnInit {

  store: StoreModel;

  constructor(private storeFormService: StoreFormService,
              private referentielService: ReferentielService,
              private tgtgService: TgtgService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.store = this.route.snapshot.data.store;
    this.referentielService.getStoreLabels().subscribe(labels => {
      this.storeFormService.initForm(labels, this.store);
    });
  }

  get form(): FormGroup {
    return this.storeFormService.form;
  }

  get detailsForm(): FormGroup {
    return this.form.get('details') as FormGroup;
  }

  get locationForm(): FormGroup {
    return this.form.get('location') as FormGroup;
  }

  submit(): void {
    console.log('>> data: ', this.form.value);
  }

  reset(): void {
    this.storeFormService.reset();
  }
}
