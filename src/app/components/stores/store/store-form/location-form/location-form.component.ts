import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BaseFormComponent } from '../base-form.component';
import { StoreFormService } from '../../../../../services/store-form.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'nc-location-form',
  templateUrl: './location-form.component.html',
  styleUrls: ['./location-form.component.scss']
})
export class LocationFormComponent extends BaseFormComponent implements OnInit {

  @Input()
  group: FormGroup;

  constructor(protected storeFormService: StoreFormService,
              protected translateService: TranslateService) {
    super(storeFormService, translateService);
  }

  ngOnInit(): void {
  }

  get addressForm(): FormGroup {
    return this.group.get('address') as FormGroup;
  }
}
