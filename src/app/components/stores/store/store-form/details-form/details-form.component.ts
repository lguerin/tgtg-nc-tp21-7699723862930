import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { ReferentielService } from '../../../../../services/referentiel.service';
import { BaseFormComponent } from '../base-form.component';
import { StoreFormService } from '../../../../../services/store-form.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'nc-details-form',
  templateUrl: './details-form.component.html',
  styleUrls: ['../store-form.component.scss', './details-form.component.scss']
})
export class DetailsFormComponent extends BaseFormComponent implements OnInit {

  categories: Array<string> = [];

  @Input()
  group: FormGroup;

  constructor(protected storeFormService: StoreFormService,
              protected translateService: TranslateService,
              private referentielService: ReferentielService) {
    super(storeFormService, translateService);
  }

  ngOnInit(): void {
    this.referentielService.getStoreCategories().subscribe(list => this.categories = list);
  }

  get labels(): FormArray {
    return this.group.get('labels') as FormArray;
  }
}
