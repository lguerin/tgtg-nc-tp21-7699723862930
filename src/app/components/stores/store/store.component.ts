import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreModel } from '../../../models/store.model';
import { ReferentielService } from '../../../services/referentiel.service';
import { Roles } from '../../../constants/roles.constant';
import { Coordinate, Spot } from '../../../../../../tgtg-nc-lib/dist/nc-map';

@Component({
  selector: 'nc-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit, AfterViewInit {

  store: StoreModel;
  categories: Array<string> = [];
  mapCenter: Coordinate;
  spots: Array<Spot> = [];

  readonly Roles = Roles;

  static formatAddress(store: StoreModel): string {
    const address = store.location.address ?? null;
    if (address) {
      return `${address.street}<br />${address.code} - ${address.city}`;
    }
    return '';
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private referentielService: ReferentielService) { }

  ngOnInit(): void {
    this.store = this.route.snapshot.data.store;
    this.initMap(this.store);
    this.referentielService.getStoreCategories().subscribe(list => this.categories = list);
  }

  ngAfterViewInit(): void {
  }

  back(): void {
    this.router.navigate(['/']);
  }

  initMap(store: StoreModel): void {
    const storeLocation = { latitude: store.location.latitude, longitude: store.location.longitude} as Coordinate;
    this.mapCenter = storeLocation;
    this.spots.push({
      text: StoreComponent.formatAddress(store),
      coordinate: storeLocation
    });
  }
}
