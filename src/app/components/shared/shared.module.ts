import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { HasRoleDirective } from '../../directives/has-role.directive';
import { NcMapModule } from 'nc-map';
import { StopPropagationDirective } from '../../directives/stop-propagation.directive';
import { TranslateModule } from '@ngx-translate/core';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [
    HasRoleDirective,
    StopPropagationDirective
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild()
  ],
  exports: [
    TranslateModule,
    StopPropagationDirective,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatChipsModule,
    HasRoleDirective,
    NcMapModule,
    MatRadioModule
  ]
})
export class SharedModule { }
